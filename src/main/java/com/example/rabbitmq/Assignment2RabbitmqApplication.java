package com.example.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication()
public class Assignment2RabbitmqApplication {

    public static void main(String[] args) throws TimeoutException, IOException {
        SpringApplication.run(Assignment2RabbitmqApplication.class, args);
    }

}

