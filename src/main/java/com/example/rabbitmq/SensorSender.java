package com.example.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class SensorSender implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;

    public SensorSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public LocalDateTime processDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(date, formatter);
    }

    public void sendMessage(SensorInfo sensorInfo) {
        System.out.println(sensorInfo);
        rabbitTemplate.convertAndSend(RabbitMQConfiguration.TOPIC_EXCHANGE_NAME, "sensors.new", sensorInfo);
    }

    @Override
    public void run(String... args) throws Exception {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "activity.txt"));
            String line = reader.readLine();
            while (line != null) {
                String[] val = line.split("\t\t");
                this.sendMessage(new SensorInfo((val[2]), processDate(val[0]), processDate(val[1])));
                Thread.sleep(200);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
